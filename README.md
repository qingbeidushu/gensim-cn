<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gensim – Python 中的主题建模</font></font></h1><a id="user-content-gensim--topic-modelling-in-python" class="anchor-element" aria-label="永久链接：gensim – Python 中的主题建模" href="#gensim--topic-modelling-in-python"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>

<p dir="auto"><a href="https://github.com/RaRe-Technologies/gensim/actions"><img src="https://github.com/RaRe-Technologies/gensim/actions/workflows/tests.yml/badge.svg?branch=develop" alt="构建状态" style="max-width: 100%;"></a>
<a href="https://github.com/RaRe-Technologies/gensim/releases"><img src="https://camo.githubusercontent.com/a8477c1b01792510cc3ae7cc2ba1073f37171dac25612acbb509d4b43279697e/68747470733a2f2f696d672e736869656c64732e696f2f6769746875622f72656c656173652f726172652d746563686e6f6c6f676965732f67656e73696d2e7376673f6d61784167653d33363030" alt="GitHub 发布" data-canonical-src="https://img.shields.io/github/release/rare-technologies/gensim.svg?maxAge=3600" style="max-width: 100%;"></a>
<a href="https://pepy.tech/project/gensim/" rel="nofollow"><img src="https://camo.githubusercontent.com/273177d39a9ede21e3b041bd42a6334309db4439585f65932b9c74e81af4a4d1/68747470733a2f2f696d672e736869656c64732e696f2f707970692f646d2f67656e73696d3f636f6c6f723d626c7565" alt="下载" data-canonical-src="https://img.shields.io/pypi/dm/gensim?color=blue" style="max-width: 100%;"></a>
<a href="https://doi.org/10.13140/2.1.2393.1847" rel="nofollow"><img src="https://camo.githubusercontent.com/f335252ffa560e135159cd81baaa41592086cff11b7909e3fe7db068043a9496/68747470733a2f2f7a656e6f646f2e6f72672f62616467652f444f492f31302e31333134302f322e312e323339332e313834372e737667" alt="DOI" data-canonical-src="https://zenodo.org/badge/DOI/10.13140/2.1.2393.1847.svg" style="max-width: 100%;"></a>
<a href="https://groups.google.com/g/gensim" rel="nofollow"><img src="https://camo.githubusercontent.com/81f978324feb83000e4b842b95e6b77ec6d3e4375f1cf248c143cd222d72247b/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f2d4d61696c696e672532304c6973742d626c75652e737667" alt="邮件列表" data-canonical-src="https://img.shields.io/badge/-Mailing%20List-blue.svg" style="max-width: 100%;"></a>
<a href="https://twitter.com/gensim_py" rel="nofollow"><img src="https://camo.githubusercontent.com/d50380012885e2a7dfa4975e3e9533898ea9035ed85b179b7a8ee8e3368c279b/68747470733a2f2f696d672e736869656c64732e696f2f747769747465722f666f6c6c6f772f67656e73696d5f70792e7376673f7374796c653d736f6369616c267374796c653d666c6174266c6f676f3d74776974746572266c6162656c3d466f6c6c6f7726636f6c6f723d626c7565" alt="跟随" data-canonical-src="https://img.shields.io/twitter/follow/gensim_py.svg?style=social&amp;style=flat&amp;logo=twitter&amp;label=Follow&amp;color=blue" style="max-width: 100%;"></a></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim 是一个用于</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">主题建模</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档索引</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
和</font><font style="vertical-align: inherit;">大型语料库</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">相似性检索的 Python 库。</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目标受众是
</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自然语言处理</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(NLP) 和</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">信息检索</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(IR) 社区。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><g-emoji class="g-emoji" alias="warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚠️</font></font></g-emoji><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">想帮忙吗？</font></font><a href="https://github.com/sponsors/piskvorky"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">赞助</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim❤️</font></font></h2><a id="user-content-️-want-to-help-out-sponsor-gensim-️" class="anchor-element" aria-label="永久链接：⚠️想要帮忙吗？ 赞助Gensim❤️" href="#️-want-to-help-out-sponsor-gensim-️"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><g-emoji class="g-emoji" alias="warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚠️</font></font></g-emoji><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim 处于稳定维护模式：我们不接受新功能，但仍然欢迎错误和文档修复！</font></font><g-emoji class="g-emoji" alias="warning"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">⚠️</font></font></g-emoji></h2><a id="user-content-️-gensim-is-in-stable-maintenance-mode-we-are-not-accepting-new-features-but-bug-and-documentation-fixes-are-still-welcome-️" class="anchor-element" aria-label="永久链接：⚠️ Gensim 处于稳定维护模式：我们不接受新功能，但仍然欢迎错误和文档修复！ ⚠️" href="#️-gensim-is-in-stable-maintenance-mode-we-are-not-accepting-new-features-but-bug-and-documentation-fixes-are-still-welcome-️"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">特征</font></font></h2><a id="user-content-features" class="anchor-element" aria-label="永久链接：特点" href="#features"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">所有算法都是与语料库大小</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无关的</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内存（可以处理大于 RAM、流式、核外的输入），</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">直观的界面</font></font></strong>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">轻松插入您自己的输入语料库/数据流（简单的流 API）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">易于使用其他向量空间算法进行扩展（简单的转换 API）</font></font></li>
</ul>
</li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">流行算法的高效多核实现，例如在线</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">潜在语义分析 (LSA/LSI/SVD)</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">潜在狄利克雷分配 (LDA)</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随机投影 (RP)</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">、
</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分层狄利克雷过程 (HDP)</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">word2vec 深度学习</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
<li><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分布式计算</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：可以</font><font style="vertical-align: inherit;">在计算机集群上运行</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">潜在语义分析</font></font></em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和
</font></font><em><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">潜在狄利克雷分配。</font></font></em><font style="vertical-align: inherit;"></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">丰富的</font></font><a href="https://github.com/RaRe-Technologies/gensim/#documentation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档和 Jupyter Notebook 教程</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果此功能列表让您摸不着头脑，您可以首先在维基百科上阅读有关</font></font><a href="https://en.wikipedia.org/wiki/Vector_space_model" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向量空间模型</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">和</font></font><a href="https://en.wikipedia.org/wiki/Latent_semantic_indexing" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无监督文档分析的</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">更多信息
。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装</font></font></h2><a id="user-content-installation" class="anchor-element" aria-label="永久链接：安装" href="#installation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该软件依赖于</font></font><a href="https://scipy.org/install/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NumPy 和 Scipy 这</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">两个用于科学计算的 Python 软件包。</font><font style="vertical-align: inherit;">您必须在安装 gensim 之前安装它们。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">还建议您在安装 NumPy 之前安装快速 BLAS 库。</font><font style="vertical-align: inherit;">这是可选的，但使用优化的 BLAS（例如 MKL、</font></font><a href="https://math-atlas.sourceforge.net/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ATLAS</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或
</font></font><a href="https://xianyi.github.io/OpenBLAS/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OpenBLAS）</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">已知可以将性能提高一个数量级。</font><font style="vertical-align: inherit;">在 OSX 上，NumPy 自动选择其 vecLib BLAS，因此您无需执行任何特殊操作。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安装最新版本的 gensim：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>    pip install --upgrade gensim</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="    pip install --upgrade gensim" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">或者，如果您下载并解压了</font></font><a href="https://pypi.org/project/gensim/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">源 tar.gz</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
包：</font></font></p>
<div class="highlight highlight-source-shell notranslate position-relative overflow-auto" dir="auto"><pre>    python setup.py install</pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="    python setup.py install" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有关替代安装模式，请参阅</font></font><a href="https://radimrehurek.com/gensim/#install" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim 正在所有</font><a href="https://github.com/RaRe-Technologies/gensim/wiki/Gensim-And-Compatibility"><font style="vertical-align: inherit;">受支持的 Python 版本</font></a><font style="vertical-align: inherit;">下
进行</font></font><a href="https://radimrehurek.com/gensim/#testing" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续测试</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">gensim 4.0.0 中删除了对 Python 2.7 的支持 - 如果必须使用 Python 2.7，请安装 gensim 3.8.3。</font></font><a href="https://github.com/RaRe-Technologies/gensim/wiki/Gensim-And-Compatibility"><font style="vertical-align: inherit;"></font></a><font style="vertical-align: inherit;"></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 gensim 如此快且内存效率如此之高？</font><font style="vertical-align: inherit;">难道不是纯粹的Python吗？Python不是又慢又贪婪吗？</font></font></h2><a id="user-content-how-come-gensim-is-so-fast-and-memory-efficient-isnt-it-pure-python-and-isnt-python-slow-and-greedy" class="anchor-element" aria-label="永久链接：为什么 gensim 如此快且内存效率如此之高？ 难道不是纯粹的Python吗？Python不是又慢又贪婪吗？" href="#how-come-gensim-is-so-fast-and-memory-efficient-isnt-it-pure-python-and-isnt-python-slow-and-greedy"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">许多科学算法可以用大型矩阵运算来表达（参见上面的 BLAS 注释）。</font><font style="vertical-align: inherit;">Gensim 通过依赖 NumPy 来利用这些低级 BLAS 库。</font><font style="vertical-align: inherit;">因此，虽然 gensim-the-top-level-code 是纯 Python，但它实际上在底层执行高度优化的 Fortran/C，包括多线程（如果您的 BLAS 是这样配置的）。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在内存方面，gensim 大量使用 Python 的内置生成器和迭代器来进行流数据处理。</font></font><a href="https://radimrehurek.com/gensim/intro.html#design-principles" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内存效率是 gensim 的设计目标</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">之一</font><font style="vertical-align: inherit;">，也是 gensim 的核心功能，而不是事后添加的东西。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档</font></font></h2><a id="user-content-documentation" class="anchor-element" aria-label="永久链接：文档" href="#documentation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<ul dir="auto">
<li><a href="https://radimrehurek.com/gensim/auto_examples/core/run_core_concepts.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">快速开始</font></font></a></li>
<li><a href="https://radimrehurek.com/gensim/auto_examples/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">教程</font></font></a></li>
<li><a href="https://radimrehurek.com/gensim/apiref.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">官方API文档</font></font></a></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持</font></font></h2><a id="user-content-support" class="anchor-element" aria-label="永久链接： 支持" href="#support"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如需商业支持，请参阅</font></font><a href="https://github.com/sponsors/piskvorky"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim 赞助</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://groups.google.com/g/gensim" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在公共Gensim 邮件列表</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上提出开放式问题</font><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://github.com/RaRe-Technologies/gensim/blob/develop/CONTRIBUTING.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在Github</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">上提出错误</font><font style="vertical-align: inherit;">，但请</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">确保遵循</font></font><a href="https://github.com/RaRe-Technologies/gensim/blob/develop/ISSUE_TEMPLATE.md"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">问题模板</font></font></a></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font><font style="vertical-align: inherit;">不是错误或未能提供所需详细信息的问题将在不进行检查的情况下关闭。</font></font></p>
<hr>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">采用者</font></font></h2><a id="user-content-adopters" class="anchor-element" aria-label="永久链接：采用者" href="#adopters"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<table>
<thead>
<tr>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">公司</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标识</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">行业</font></font></th>
<th><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim的使用</font></font></th>
</tr>
</thead>
<tbody>
<tr>
<td><a href="https://rare-technologies.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">稀有技术</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/rare.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/rare.png" alt="稀有的" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">机器学习和自然语言处理咨询</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim 的创造者——这就是我们！</font></font></td>
</tr>
<tr>
<td><a href="http://www.amazon.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚马逊</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/amazon.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/amazon.png" alt="亚马逊" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">零售</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档相似度。</font></font></td>
</tr>
<tr>
<td><a href="https://github.com/NIHOPA/pipeline_word2vec"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">美国国立卫生研究院</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/nih.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/nih.png" alt="NIH" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">健康</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 word2vec 处理资助和出版物。</font></font></td>
</tr>
<tr>
<td><a href="http://www.cisco.com/c/en/us/products/security/index.html" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">思科安全</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/cisco.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/cisco.png" alt="思科" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大规模欺诈检测。</font></font></td>
</tr>
<tr>
<td><a href="http://www.mindseyesolutions.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">心灵的眼睛</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/mindseye.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/mindseye.png" alt="心灵的眼睛" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">合法的</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">法律文件中的相似之处。</font></font></td>
</tr>
<tr>
<td><a href="http://www.channel4.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">频道4</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/channel4.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/channel4.png" alt="频道4" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">媒体</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">推荐引擎。</font></font></td>
</tr>
<tr>
<td><a href="http://talentpair.com" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人才对</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/talent-pair.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/talent-pair.png" alt="人才对" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人力资源</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">高接触招聘中的候选人匹配。</font></font></td>
</tr>
<tr>
<td><a href="http://www.juju.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">朱朱</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/juju.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/juju.png" alt="朱朱" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人力资源</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供非明显相关的工作建议。</font></font></td>
</tr>
<tr>
<td><a href="https://www.tailwindapp.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">顺风</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/tailwind.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/tailwind.png" alt="顺风" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">媒体</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将有趣且相关的内容发布到 Pinterest。</font></font></td>
</tr>
<tr>
<td><a href="https://issuu.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发行</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/issuu.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/issuu.png" alt="发行" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">媒体</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim 的 LDA 模块是我们对每个上传的出版物进行分析的核心，以找出其全部内容。</font></font></td>
</tr>
<tr>
<td><a href="http://www.searchmetrics.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">搜索指标</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/search-metrics.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/search-metrics.png" alt="搜索指标" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内容营销</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Gensim word2vec 用于搜索引擎优化中的实体消歧。</font></font></td>
</tr>
<tr>
<td><a href="https://12k.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12K研究</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/12k.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/12k.png" alt="12k" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">媒体</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">媒体文章的文档相似性分析。</font></font></td>
</tr>
<tr>
<td><a href="http://www.stillwater-sc.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">斯蒂尔沃特超级计算</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/stillwater.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/stillwater.png" alt="死水" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">硬件</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">文档理解以及与 word2vec 的关联。</font></font></td>
</tr>
<tr>
<td><a href="https://www.siteground.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">站点地面</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/siteground.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/siteground.png" alt="场地" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">虚拟主机</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一个使用不同嵌入模型和相似性的集成搜索引擎，包括 word2vec、WMD 和 LDA。</font></font></td>
</tr>
<tr>
<td><a href="https://www.capitalone.com/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第一资本</font></font></a></td>
<td><a target="_blank" rel="noopener noreferrer" href="/piskvorky/gensim/blob/develop/docs/src/readme_images/capitalone.png"><img src="/piskvorky/gensim/raw/develop/docs/src/readme_images/capitalone.png" alt="首都" style="max-width: 100%;"></a></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">金融</font></font></td>
<td><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用于客户投诉探索的主题建模。</font></font></td>
</tr>
</tbody>
</table>
<hr>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">引用gensim</font></font></h2><a id="user-content-citing-gensim" class="anchor-element" aria-label="永久链接：引用 gensim" href="#citing-gensim"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"></font><a href="https://scholar.google.com/citations?view_op=view_citation&amp;hl=en&amp;user=9vG_kV0AAAAJ&amp;citation_for_view=9vG_kV0AAAAJ:NaGl4SEjCO4C" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在学术论文和论文中引用 gensim</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">时</font><font style="vertical-align: inherit;">，请使用此 BibTeX 条目：</font></font></p>
<div class="snippet-clipboard-content notranslate position-relative overflow-auto"><pre class="notranslate"><code>@inproceedings{rehurek_lrec,
      title = {{Software Framework for Topic Modelling with Large Corpora}},
      author = {Radim {\v R}eh{\r u}{\v r}ek and Petr Sojka},
      booktitle = {{Proceedings of the LREC 2010 Workshop on New
           Challenges for NLP Frameworks}},
      pages = {45--50},
      year = 2010,
      month = May,
      day = 22,
      publisher = {ELRA},
      address = {Valletta, Malta},
      note={\url{http://is.muni.cz/publication/884893/en}},
      language={English}
}
</code></pre><div class="zeroclipboard-container">
    <clipboard-copy aria-label="Copy" class="ClipboardButton btn btn-invisible js-clipboard-copy m-2 p-0 tooltipped-no-delay d-flex flex-justify-center flex-items-center" data-copy-feedback="Copied!" data-tooltip-direction="w" value="@inproceedings{rehurek_lrec,
      title = {{Software Framework for Topic Modelling with Large Corpora}},
      author = {Radim {\v R}eh{\r u}{\v r}ek and Petr Sojka},
      booktitle = {{Proceedings of the LREC 2010 Workshop on New
           Challenges for NLP Frameworks}},
      pages = {45--50},
      year = 2010,
      month = May,
      day = 22,
      publisher = {ELRA},
      address = {Valletta, Malta},
      note={\url{http://is.muni.cz/publication/884893/en}},
      language={English}
}" tabindex="0" role="button">
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-copy js-clipboard-copy-icon">
    <path d="M0 6.75C0 5.784.784 5 1.75 5h1.5a.75.75 0 0 1 0 1.5h-1.5a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-1.5a.75.75 0 0 1 1.5 0v1.5A1.75 1.75 0 0 1 9.25 16h-7.5A1.75 1.75 0 0 1 0 14.25Z"></path><path d="M5 1.75C5 .784 5.784 0 6.75 0h7.5C15.216 0 16 .784 16 1.75v7.5A1.75 1.75 0 0 1 14.25 11h-7.5A1.75 1.75 0 0 1 5 9.25Zm1.75-.25a.25.25 0 0 0-.25.25v7.5c0 .138.112.25.25.25h7.5a.25.25 0 0 0 .25-.25v-7.5a.25.25 0 0 0-.25-.25Z"></path>
</svg>
      <svg aria-hidden="true" height="16" viewBox="0 0 16 16" version="1.1" width="16" data-view-component="true" class="octicon octicon-check js-clipboard-check-icon color-fg-success d-none">
    <path d="M13.78 4.22a.75.75 0 0 1 0 1.06l-7.25 7.25a.75.75 0 0 1-1.06 0L2.22 9.28a.751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018L6 10.94l6.72-6.72a.75.75 0 0 1 1.06 0Z"></path>
</svg>
    </clipboard-copy>
  </div></div>
</article></div>
